load "$NCL_DIR/common.ncl"

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Whilst this approach works with ensemble size of 5,;;;
;;; It completely crashes my laptop with ensemble > 5;;;;;
;;; Need to revise process....;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; SETTINGS ;;;;
PLAY_WITH_DEFAULT=False
INITIAL_PARAMETER_SAMPLE=True
VERBOSE=True
ENFORCE_RECALCULATION=False; Be careful setting this as true. It will take time...

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; FUNCTIONS REQUIRED FOR COMPUTATION ;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
undef("iterate")
function iterate(bkgd_param,precc_param,co2_param,fdbk_param,tauv,taun,sigma,years,precc,co2,VERBOSE)
begin
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;This function runs 100 instances of the model with set parameters.
  ;The first 7 inputs are the 7 different parameters.
  ;    Each of the parameters only be 1d-arrays AND OF THE SAME SIZE!
  ;The final 3 inputs are years,precc,co2 - which define the forcing values
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Initialise arrays
  nu=new((/dimsizes(sigma),100,dimsizes(years)/),float,-999.) 
  nu(:,:,0)=1. ;set the initial conditions as 1 (forest)
  N=new((/dimsizes(sigma),100,dimsizes(years)/),float,-999.)
  N(:,:,0)=0. ;set the initial conditions of noise as 0
  ;cast parameters for increased readability
  a=conform_dims((/dimsizes(sigma),100/),bkgd_param,0)
  b=conform_dims((/dimsizes(sigma),100/),precc_param,0)
  c=conform_dims((/dimsizes(sigma),100/),co2_param,0)
  d=conform_dims((/dimsizes(sigma),100/),fdbk_param,0)
  s=conform_dims((/dimsizes(sigma),100/),sigma,0)
  tn=conform_dims((/dimsizes(sigma),100/),taun,0)
  tv=conform_dims((/dimsizes(sigma),100/),tauv,0)
  ;sample white noise
  random_setallseed(36484749, 9494848)
  noise=random_normal(0,1,dimsizes(nu))
  ;step through time
  do yr=0,dimsizes(years)-2
    ;note. by enforcing Dt=1, then the Dt and sqrt(Dt) disappear
    r=a+b*precc(yr)+c*co2(yr)+d*nu(:,:,yr)+N(:,:,yr)
    nu(:,:,yr+1)=nu(:,:,yr)+(tanh(r)-nu(:,:,yr))/tv
    N(:,:,yr+1)=N(:,:,yr)+(s*noise(:,:,yr)-N(:,:,yr))/tn
    if VERBOSE.and.(yr%10000.eq.1) then
      print("We're on year "+yr+": r="+r(0,0)+", nu="+nu(0,0,yr)+", N="+N(0,0,yr)+\
              " with: a="+a(0,0)+", b*precc="+(b(0,0)*precc(yr))+\
              ", (c*co2)="+(c(0,0)*co2(yr))+", dv="+d(0,0)*nu(0,0,yr)+", N(yr-1)="+N(0,0,yr-1))
    end if
  end do        
  return(nu)
end;function iterate

undef("get_centennial_averages")
function get_centennial_averages(filename,bkgd_param,precc_param,co2_param,fdbk_param,tauv,taun,sigma,ENFORCE_RECALCULATION,VERBOSE)
begin
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;This function will get the centennial averages for a selection of the 7 parameters 
  ;The first input is a filename (inc path if needed and ending with .nc) 
  ;    The program will try to open this file, otherwise it will create it! 
  ;The other 7 inputs are the 7 different parameters.
  ;    Each of the parameters only be 1d-arrays AND OF THE SAME SIZE!
  ;The ENFORCE_RECALCULATION flag will recreate the file. Beware may take several hours.
  ;The VERBOSE flag will tell you what's happening.
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;;;;CHECK IF FILE EXISTS. IF SO, READ FROM IT.
  CREATE_TIMESERIES=True          
  setfileoption("nc", "Format",  "NetCDF4")
  if .not.ENFORCE_RECALCULATION then
    if isfilepresent(filename) then
      centavgf = addfile(filename, "r")
      if isfilevar(centavgf,"cent_avg_ts") then
        cent_avg_ts=centavgf->cent_avg_ts
        bkgd_param_test=centavgf->bkgd_param
        if dimsizes(bkgd_param_test).eq.dimsizes(bkgd_param) then
          if all(bkgd_param_test.eq.bkgd_param) then
            precc_param_test=centavgf->precc_param
            co2_param_test=centavgf->co2_param
            fdbk_param_test=centavgf->fdbk_param
            tauv_test=centavgf->tauv
            taun_test=centavgf->taun
            sigma_test=centavgf->sigma
            if (all(precc_param_test.eq.precc_param)).and.(all(co2_param_test.eq.co2_param)).and.\
              (all(fdbk_param_test.eq.fdbk_param)).and.(all(tauv_test.eq.tauv)).and.\
              (all(taun_test.eq.taun)).and.(all(sigma_test.eq.sigma)) then
              if VERBOSE then
                print(" ")
                print(" File is fine. Loaded and continuing")
              end if
              CREATE_TIMESERIES=False          
            else
              delete([/precc_param_test,co2_param_test,fdbk_param_test,tauv_test,taun_test,sigma_test/])
            end if
          end if;all parameter values correct
        end if;correct dimsizes
        if CREATE_TIMESERIES then
          delete([/cent_avg_ts,bkgd_param_test,centavgf/])
        end if;CREATE_TIMESERIES
      end if;isfilevar(nuf,"nu_default")
    end if;isfilepresent("default_params_nu.nc")
  end if;ENFORCE_RECALCULATION

  ;;;;IF THE DATA ISN'T FOR THE CORRECT PARAMETERS THEN WE NEED TO MAKE IT!
  if CREATE_TIMESERIES then
    if VERBOSE then
      print(" ")
      print("I need to create the requested parameter settings and perform the centennial averaging!")
      print("Please bear with me: this may take some time")
      print(" ")
    end if
    ;Read in observed forcing data from file...
    if VERBOSE then
      print(" Reading in the forcing datasets")
    end if
    bl_orbit=readAsciiTable("~/Documents/obs/Solar/BergerLoutre1991.csv",9,"float",2)
    time_bl=1000*abs(bl_orbit(:,0)) ;need to convert from ka to years BP
    prec_bl=bl_orbit(:,4)
    epica=readAsciiTable("~/Documents/obs/PaleoObs/Pleistocene/EPICA_BBC.txt",4,"float",13)
    time_epica=epica(:,0)-50 ;need to convert from yr2000 to BP
    co2_epica=epica(:,3)
    co2_forcing_epica=5.35*log(co2_epica/278);radiative forcing change from preindustrial
    ;set up some years arrays
    ; years_230ka is in units of year Before Present (i.e. 1950)
    years_230ka=230000-ispan(1,230000,1);year counter
    ; cents_230ka is an array for plotting smoothed data.
    ; units is ka (thousand years BP)
    cents_230ka=(tofloat(2300-ispan(1,2300,1))/10.)+0.05;
    cents_230ka@units="ka"

    ;Interpolate the CO2 and Orbits into annual years
    ;  the piecewise linear interpolation routine requires coordinates increasing
    if VERBOSE then
      print(" Interpolating the forcing datasets to annual")
    end if
    co2_record_tmp=linint1(time_epica,co2_forcing_epica,False,years_230ka(::-1),0)
    precc_record_tmp=linint1(time_bl,prec_bl,False,years_230ka(::-1),0)
    ;  so we created them backwards and flip now.
    co2_yearly=co2_record_tmp(::-1)
    precc_yearly=precc_record_tmp(::-1)
    delete([/co2_record_tmp,precc_record_tmp/])
    ;tidy up so far to clear memory
    co2_record_tmp=linint1(time_epica,co2_forcing_epica,False,(cents_230ka(::-1)*1000),0)
    precc_record_tmp=linint1(time_bl,prec_bl,False,(cents_230ka(::-1)*1000),0)
    co2=co2_record_tmp(::-1)
    co2!0="time"
    co2&time=cents_230ka
    precc=precc_record_tmp(::-1)
    precc!0="time"
    precc&time=cents_230ka
    delete([/bl_orbit,time_bl,prec_bl,epica,time_epica,co2_epica,\
            co2_forcing_epica,co2_record_tmp,precc_record_tmp/])

    ;And now perform the actual data creation...
    if VERBOSE then
      print("STARTING TO ITERATE NOW")
    end if
    yearly_nu=iterate(bkgd_param,precc_param,co2_param,fdbk_param,tauv,taun,sigma,\
                      years_230ka,precc_yearly,co2_yearly,VERBOSE)
    if VERBOSE then
      print("FINISHED ITERATING FINALLY")
    end if

    ;calculate centennial averages
    if VERBOSE then
      print(" Calculating the centennial averages.")
    end if
    cent_avg_ts=dim_avg(reshape(yearly_nu,(/dimsizes(bkgd_param),100,dimsizes(cents_230ka),100/)))
    cent_avg_ts!0="parameter_setting"
    cent_avg_ts&parameter_setting=ispan(1,dimsizes(bkgd_param),1)
    cent_avg_ts!1="instances"
    cent_avg_ts&instances=ispan(1,100,1)
    cent_avg_ts!2="time"
    cent_avg_ts&time=cents_230ka


    ;Write these variables all out to file
    if VERBOSE then
      print(" Writing out the centennial averages")
    end if
    system("/bin/rm -f "+filename)
    centavgf = addfile(filename, "c")
    centavgf->cent_avg_ts=cent_avg_ts
    if VERBOSE then
      print(" Writing out the parameter values")
    end if
    centavgf->bkgd_param=bkgd_param
    centavgf->precc_param=precc_param
    centavgf->co2_param=co2_param
    centavgf->fdbk_param=fdbk_param
    centavgf->tauv=tauv
    centavgf->taun=taun
    centavgf->sigma=sigma
    if VERBOSE then
      print(" Writing out the forcing timeseries")
    end if
    centavgf->co2=co2
    centavgf->precc=precc
  end if
  return(cent_avg_ts)
end;function get_centennial_averages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

undef("compute_num_sapropel_metric")
function compute_num_sapropel_metric(cent_avg_ts,isSapThreshold,VERBOSE)
begin
  ; Function to calculate number of sapropels seen across the record (230ka to present).
  ; isSapThreshold = tunable parameter: an nu ge isSapThreshold represents a sapropel
  ;    define sapropel as a run of greater than 1500 years (150 centuries)
  ds=dimsizes(cent_avg_ts)
  n_param_settings=ds(0)
  run_length_histo=dim_numrun_n(toint(where(cent_avg_ts.ge.isSapThreshold,1,0)),1,2)
  nsaps=dim_sum(run_length_histo(:,:,15:))
  p_nsaps=new((/n_param_settings,16/),float)
  p_nsaps@long_name="Proportion with sapropel quantity"
  p_nsaps!0="parameter_setting"
  p_nsaps!1="nsaps"
  nsaps_array=ispan(0,15,1)
  nsaps_array@long_name="Number of sapropels between 230-0ka"
  p_nsaps&nsaps=nsaps_array
  p_nsaps@isSapThreshold=isSapThreshold
  do i=0,15
    p_nsaps(:,i)=dim_num(nsaps.eq.i)
  end do
  if VERBOSE then
    print("For the first parameter settting, the proportion having 7 sapropels is "+(p_nsaps(0,7)/100.))
  end if
  return(p_nsaps)
end; function compute_num_sapropel_metric
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

undef("compute_sapropel_startends_metric")
function compute_sapropel_startends_metric(cent_avg_ts,isSapThreshold,ZeiglerDatingError,VERBOSE)
begin
  ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ; Function to calculate the proportion of model instances that are
  ;  A. starting a sapropel at Ziegler's bottom dates (neglecting Holocene one of course)
  ;  B. ending a sapropel at Ziegler's top dates (neglecting Holocene one of course)
  ; isSapThreshold = tunable parameter: an nu ge isSapThreshold represents a sapropel
  ; ZeiglerDatingError = Error attached Ziegler Dates (in kyr). Currently coded as being invariant in time
  ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;set up output array
  ds=dimsizes(cent_avg_ts)
  n_param_settings=ds(0)
  p_Zieg_startends=new((/n_param_settings,2,6/),float) 
  p_Zieg_startends!0="parameter_setting"
  p_Zieg_startends!1="start_end_01"
  p_Zieg_startends!2="Zeigler_sapropel_number"
  p_Zieg_startends&Zeigler_sapropel_number=(/3,4,5,6,7,8/)
  p_Zieg_startends@isSapThreshold=isSapThreshold
  p_Zieg_startends@ZeiglerDatingError=ZeiglerDatingError

  ;Ziegler Sapropel Dates (from Table 1) and ignoring Precursory S3 and Precursory S4
  Zieg_start=(/10.2,83.9,104,129.5,178.5,198.5,224.1/);units of ka
  Zieg_end=(/6.5,77.3,99.6,121.4,165.5,191.9,209.5/)

  ;define transitioning
  isSapStartEnd=where((cent_avg_ts.gt.(isSapThreshold-0.5)).and.(cent_avg_ts.le.isSapThreshold),1,0)
     ;choosing integer array not boolean for later dim_sum_n command instead of looping with an "any" command 
  
  ;loop over Ziegler Sapropels
  do i=0,5
    ind_this_start=ind((cent_avg_ts&time.ge.(Zieg_start(i+1)-ZeiglerDatingError)).and.\
                      (cent_avg_ts&time.lt.(Zieg_start(i+1)+ZeiglerDatingError)))
    ind_this_end=ind((cent_avg_ts&time.ge.(Zieg_end(i+1)-ZeiglerDatingError)).and.\
                      (cent_avg_ts&time.lt.(Zieg_end(i+1)+ZeiglerDatingError)))
    p_Zieg_startends(:,0,i)=(dim_num_n(dim_sum_n(isSapStartEnd(:,:,ind_this_start),2).ge.1,1))/100.
    p_Zieg_startends(:,1,i)=(dim_num_n(dim_sum_n(isSapStartEnd(:,:,ind_this_end),2).ge.1,1))/100.
    delete([/ind_this_start,ind_this_end/])
    if VERBOSE then
      fmtstr="For the first parameter settting, the proportion having a sapropel "
      print(fmtstr+"start at "+Zieg_start(i+1)+"ka is "+p_Zieg_startends(0,0,i))
      print(fmtstr+"end at "+Zieg_end(i+1)+"ka is "+p_Zieg_startends(0,1,i))
    end if 
  end do
  return(p_Zieg_startends)
end;function compute_metrics

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; START OF COMPUTATION ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Create some piccies and metrics for the default parameter setting
if PLAY_WITH_DEFAULT then
  default_params=(/0.,-50.,1.,1.,5.,5.,1./);bkgd_param,precc_param,co2_param,fdbk_param,tauv,taun,sigma
  default_filename="default_params_nu.nc"

  nu_default_cents=get_centennial_averages(default_filename,default_params(0),\
                   default_params(1),default_params(2),default_params(3),default_params(4),\
                   default_params(5),default_params(6),ENFORCE_RECALCULATION,VERBOSE)
  ;read in other variables from file...
  defaultf = addfile(default_filename, "r")
  co2=defaultf->co2
  precc=defaultf->precc
  cents_230ka=co2&time

  ;Ziegler Sapropel Dates (from Table 1) and ignoring Precursory S3 and Precursory S4
  Zieg_start=(/10.2,83.9,104,129.5,178.5,198.5,224.1/);units of ka
  Zieg_end=(/6.5,77.3,99.6,121.4,165.5,191.9,209.5/)

  ;plot some things
  wks = gsn_open_wks("pdf","nu_default_timeseries")
  res=True   
     res@gsnFrame=False
     res@gsnDraw=False
     res@tiXAxisString          = "Thousands of Years (BP)"
     res@xyLineThicknessF = 3
     res@gsnMaximize=True
     res@vpKeepAspect = False
     res@vpHeightF=0.25
     res@tiYAxisString = "Veg (1=forest,-1=desert)"
     res@trXMinF = 0.
     res@trXMaxF= 230.
     res@gsnCenterString = "Timeseries of 100 instances of the default parameters"
     res@gsnXRefLine=(/10.2,83.9,104,129.5,178.5,198.5,224.1,6.5,77.3,99.6,121.4,165.5,191.9,209.5/)
     res@gsnXRefLineColor=(/"green","green","green","green","green","green","green",\
                            "blue","blue","blue","blue","blue","blue","blue"/)
  plot2 = gsn_csm_xy(wks,cents_230ka,nu_default_cents(0,:,:),res)  
     res@gsnCenterString = " "
     res@tiYAxisString = "CO~B~2~N~ RF in W/m~S~2~N~ (black)"
     resR=res
     resR@xyDashPatterns=1
     resR@xyLineColors="red"
     resR@tiYAxisString = "Preccession (red)"
  plot1 = gsn_csm_xy2(wks,cents_230ka,co2,precc,res,resR)  
  attachres = True
  attachres@gsnAttachPlotsXAxis = True ; attaches along x-axis  resP=True
  resP=True
  resP@gsnMaximize=True
  foo=gsn_attach_plots(plot2,plot1,attachres,True)
  gsn_panel(wks,plot2,(/1,1/),resP)
  delete([/wks,res/])
  ;compute some metrics
  pse=compute_sapropel_startends_metric(nu_default_cents,0.,0.5,VERBOSE)
  p_nsaps=compute_num_sapropel_metric(nu_default_cents,0.,VERBOSE)  
  print("The proportion having 7 or 8 sapropels is "+(sum(p_nsaps(0,7:8))/100.))
end if ; PLAY_WITH_DEFAULT

;Create initial parameter sample
if INITIAL_PARAMETER_SAMPLE then 
  n_samples=100;n_samples over broad ranges with uniform distribution
  random_setallseed(36484749, 9494847);by setting the seed I know that I'll regenerate the same random settings.
  IPS_bkgd_param=random_uniform(-3.,3.,n_samples)
  IPS_precc_param=random_uniform(-150.,0.,n_samples)
  IPS_co2_param=random_uniform(0.,5.,n_samples)
  IPS_fdbk_param=random_uniform(0.,2.,n_samples)
  IPS_tauv=random_uniform(1.,10.,n_samples)
  IPS_taun=random_uniform(1.,10.,n_samples)
  IPS_sigma=random_uniform(0.,2.,n_samples)
  IPS_filename="initial_parameter_sample_nu.nc"
  if VERBOSE then 
    print(" ")
    print("Generated the random numbers")
  end if


  if VERBOSE then 
    begTime = get_cpu_time()
  end if
  IPS_nu=get_centennial_averages(IPS_filename,IPS_bkgd_param,\
                   IPS_precc_param,IPS_co2_param,IPS_fdbk_param,IPS_tauv,\
                   IPS_taun,IPS_sigma,False,VERBOSE)
  if VERBOSE then
    print(" Time to read in or calculate averages: " + (get_cpu_time() - begTime)+ " seconds")
  end if 

  ;read in other variables from file...
  IPSf = addfile(IPS_filename, "r")
  co2=IPSf->co2
  precc=IPSf->precc

  ;Compute some metrics for these ensemble members
  if VERBOSE then 
    begTime = get_cpu_time()
    print(" ")
    print("Computing some metrics")
  end if
  IPS_pse=compute_num_sapropel_metric(IPS_nu,0.,VERBOSE)
  if VERBOSE then
    print(" Time to compute metrics: " + (get_cpu_time() - begTime)+ " seconds")
  end if 
  seven_saps_as_most_prob=ind(IPS_pse(:,7).ge.50)
  print("Only "+dimsizes(seven_saps_as_most_prob)+" have 7 sapropels as the most likely number. They are:")
  print("  ind: "+seven_saps_as_most_prob+" ("+IPS_bkgd_param(seven_saps_as_most_prob)+", "+\
        IPS_precc_param(seven_saps_as_most_prob)+", "+IPS_co2_param(seven_saps_as_most_prob)+", "+\
        IPS_fdbk_param(seven_saps_as_most_prob)+", "+IPS_tauv(seven_saps_as_most_prob)+", "+\
        IPS_taun(seven_saps_as_most_prob)+", "+IPS_sigma(seven_saps_as_most_prob)+")")
end if ; INITIAL_PARAMETER_SAMPLE

;TO DO....
;immediate (for each parameter setting)
;DONE 1. interpolate forcings onto yearly timesteps
;DONE 2. run iterate code to check it works
;DONE 3. sum across noise to get probability
;DONE 4. calculate probable start, end and length of resultant sapropels
;DONE 5. compare this to Ziegler (except first)
;longer
;A. sample parameters (wrap FORTRAN Latin-Hypercube code?)
;B. Compute combined probability of receiving observations from parameter setting
;C. Use this as a weight to forecast length of most recent sapropel
;D. Do I need an emulator to cover parameter space?
    
    
