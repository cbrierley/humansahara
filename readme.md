This selection of routines was developed to create an idealised model of the Sahara. They were written when I was writing "Humans delayed the collapse of the Green Sahara" by Brierley et al. This is not intended to be a released software package - rather it is an open record of my ongoing research using the model. Please contact me if you'd like some help understanding the scripts.

*If you do want to recreate the figures in the manuscript follow the instructions below*

The major control script is called mk_figures.ncl and is run from the directory using:

    ncl -n mk_figures.ncl

This relies on a series of functions and procedures to actually run the model that are stored in *SaharaModel_functions.ncl*. I use version 6.3 of NCL. I suspect that the scripts might work with version 6.0+, but have not tested it on those older version of the software.

The necessary data files to the run *mk_figures.ncl* are included in this single directory.

When setting any of the SHANAHAN, TIMESERIES_230ka or HOLOCENE flags, the script will first check to see if a couple of netcdf's containing model output exist in the directory. If not it will create them - this will take some time (~1 hour). You can force the scripts to recalculate all the model output by setting ENFORCE_RECALCULATION=True. If the files do exist, it will perform some simple checks to make sure that the settings are the same as in the script. Only then start the plotting routines.