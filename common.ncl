;COMMON.ncl 
; constants to be made glocally available
; use unique [uncommon] names
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
;load "~/Documents/ncl/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/popRemap.ncl"

print("loading COMMON constants")
GRAV_C = 9.80665 ; gravity at 45 deg lat used by the WMO
OMEGA_C = 7.292e-05 ; earth's angular vel
RE_C = 6.37122e06 ; average radius of earth in m
PI_C = 4.*atan(1.) ;Pi
RAD_C = PI_C/180.  ;conversion of degrees to radians
RHO_OCEAN_C = 1026 ;kg/m^3
RHO_WATER_C = 1000
SIGMA_C = 5.67e-8 
K_BOLTZMANN_C = 1.381e-23 
PLANCK_C = 6.626e-34
SOLAR_CONST_C = 1365.
CP_WATER_C = 4218. ;joules per kg per Kelvin
CFUS_WATER_C = 3.337e5
CVAP_WATER_C = 2.501e6 
MOL_WT_WATER_C = 18.015 
CP_ICE_C = 2106. 
RHO_ICE_C = 917.
FREEZE_PT_C = 273.15 
TRIPLE_PT_C = 273.16 
BOIL_PT_C = 373.15 
PERF_GAS_CONST_C = 8.314
GAS_VOL_C = 0.02241 
AVOGADRO_C = 6.022e+26 
SPEED_LIGHT_C = 2.9979e+8 
P_REF_C = 101325.
P_REF_UM_C = 100000. 
MOL_WT_AIR_C = 28.964 
GAS_CONST_AIR_C = 287.05
CP_AIR_C = 1005.
CV_AIR_C = 718.
CP_BY_CV_C = 1.4 
RHO_AIR_STP_C = 1.293 
VISCOSITY_AIR_STP_C = 1.73e-5
THERMAL_COND_AIR_C = 0.024 
SECS_DAY_C = 86400 
KNOTS_TO_MS = 1./1.94384449 ;conversion of knots to m/s
CP_SEAWATER_C = 3993 ; joules per kg per Kelvin at 20oC

;function to calculate the coriolis parameter from a latitude in degrees
undef("coriolis")
function coriolis (angle:numeric)
begin
  return (2.*OMEGA_C*sin(angle*RAD_C)) ; use common variables
end

;function to calculate the beta parameter from a latitude in degrees
undef("beta_parameter")
function beta_parameter (angle:numeric)
begin
  return(2.*OMEGA_C*cos(angle*RAD_C)/RE_C)
end

;resets the _FillValue to something different
undef("ResetMissingTo")
function ResetMissingTo (Array,Replacement_Value)
begin
   ONED=ndtooned(Array)
   missing=ONED@_FillValue
   delete(ONED@_FillValue)
   was_missing=ind(ONED.eq.missing)
   if .not. all(ismissing(was_missing)) then
    ONED(was_missing)=Replacement_Value
   end if 
   Array = onedtond(ONED, dimsizes(Array))
  return(Array)
  delete(was_missing)
  delete(ONED)
  delete(missing)
end

;creates a 2D array of the surface area
undef("SurfaceArea")
function SurfaceArea(lat,lon,gw)
begin
  nlon=dimsizes(lon)
  nlat=dimsizes(lat)
  Area=new((/nlat,nlon/),typeof(gw))
  Area!0="lat"
  Area&lat=lat   
  Area!1="lon"
  Area&lon=lon   
  dlon=lon(1)-lon(0)
   do i = 0, nlat-1
       Area(i,:)=(dlon*PI_C*RE_C*RE_C/180.)*gw(i)
   end do  
  return(Area)
end

;Program to run up a quick plot of a field 
undef("roughlatlonplot")
procedure roughlatlonplot(Field)
begin

 if dimsizes(dimsizes(Field)).ne.2 then
  print("Input only 2D latxlon arrays, please.")
  return
 end if

 if isatt(Field,"long_name") then
   title="rough plot of "+Field@long_name
 else
   title="rough plot"
 end if

 wks=gsn_open_wks("X11",title) 
 gsn_define_colormap(wks,"rainbow")
 res                       = True                ; plot mods desired
 res@cnFillMode            = "RasterFill"       ; raster plot
 res@cnFillOn              = True
 if isatt(Field,"cnLinesOn") then
   res@cnLinesOn = Field@cnLinesOn
 else
   res@cnLinesOn             = True
 end if
 res@cnLineLabelsOn        = True
 res@cnLevelSelectionMode  = "AutomaticLevels" 
 res@gsnSpreadColors	  = True 
 res@gsnDraw               = True
 res@gsnFrame              = True
 res@gsnAddCyclic          = False
 if isatt(Field,"valid_range") then
   res@cnLevelSelectionMode  = "ManualLevels" 
   res@cnMaxLevelValF       =  Field@valid_range(1)
   res@cnMinLevelValF       =  Field@valid_range(0)
   res@cnLevelSpacingF      =  (Field@valid_range(1)-Field@valid_range(0))/10.
 end if
 if isatt(Field,"mpMinLatF") then
     res@mpMinLatF    = Field@mpMinLatF 
     res@mpMaxLatF    = Field@mpMaxLatF
     res@mpMinLonF    = Field@mpMinLonF 
     res@mpMaxLonF    = Field@mpMaxLonF
     res@mpCenterLonF = avg((/res@mpMinLonF,res@mpMaxLonF/))
 end if
 plot = gsn_csm_contour_map(wks,Field,res)
 delete(wks)
end

; *******************************************************************
; THIS IS A VERSION OF DIM_AVG_WRAP THAT RETURNS _FILLVALUE IF ANY OF
; THE CONTRIBUTING DATA ARE MISSING. EQUIVALENT TO SETTING
; MDTOLERANCE TO 0 IN PP_WAVE 
; CMB 11TH AUG 2009

undef ("dim_avg_NotIfMiss")
function dim_avg_NotIfMiss (x:numeric)     
local xave, dimx, Ndx, Ndx1,logicalmask
begin
 xave = dim_avg(x)          ; arithmetic ave [no meta data] 

 logicalmask=where(ismissing(x),1,0); create a 1/0 mask
 xmiss = dim_sum(logicalmask);integrate this mask to get same dimensions 
 xave = where(xmiss.ge.1,xave@_FillValue,xave)

 dimx = dimsizes(x)         ; size of each dimension
 Ndx  = dimsizes(dimx)      ; number of dimensions
 copy_VarCoords_1 (x, xave) ; one less dimension 
                                       ; add an extra attribute
 Ndx1 = Ndx-1                          ; last dimension
 xave@average_op_ncl = dimWrapString("dim_avg",x,Ndx1)
 
 return (xave)
end

undef("abs_vort")
function abs_vort (u:numeric, v:numeric, lat:numeric)
;lat is probably u&lat
;taken from uv2vrG_Wrap
local vrt,f
begin
 vrt = uv2vrG (u,v)
 copy_VarMeta (u, vrt)
 vrt@long_name = "absolute vorticity"
 vrt@units     = "1/s"        ; assume u,v are m/s
 f=coriolis(lat) ;calculate coriolis parameter from above
 if (typeof(f).eq."double").and.(typeof(vrt).eq."float") then
   vrt=(/vrt+conform(vrt,doubletofloat(f),dimsizes(dimsizes(vrt))-2)/)
 else  
   vrt=(/vrt+conform(vrt,f,dimsizes(dimsizes(vrt))-2)/)
 end if
 ;this assumes that u is (?,lat,lon)
 return (vrt)
end

function OMEGA_TO_W (omega, p, t)
; first order conversion: w = -omega/(density*gravity)
; omega@units = "Pa/sec"
; p@units = "Pa"
; t@units = "K"
local rank_omega, rank_t, rank_p, RGAS, GRAV, rho, w
begin
   rank_omega = dimsizes( dimsizes(omega) )
   rank_t = dimsizes( dimsizes( t ) )
   rank_p = dimsizes( dimsizes( p ) )
   if (rank_omega.ne.rank_t .or. rank_omega.ne.rank_p) then
       print("OMEGA_TO_W: omega, p, t must be the same rank")
       exit
   end if
   RGAS = 287. ; J/(kg-K) => m2/(s2 K)
   GRAV = 9.8 ; m/s2
   rho = p/(RGAS*t) ; density => kg/m3
   w = -omega/(rho*GRAV)
   w@long_name = "vertical velocity"
   w@units = "m/s"
   copy_VarCoords( omega, w)
   return( w )
end

undef("calc_moist_entropy")
function calc_moist_entropy (T,p,q)
;This function calculates the moist entropy using eq. 4 of Emanuel et al. (2008) in BAMS
;This will be used probably be used to mainly to calculate Chi in Kerry's New GPI index.
;The inputs are: 
;   Temperature in Kelvin,
;   Pressure in Pa
;   Specific Humidity (kg/kg)
;(it only works if all the inputs are the same type eg all float)
;The equation is s=cp*ln(T)-Rd*ln(p)+Lv*q/T-Rv*q*lnH 
;The output is a moist entropy in J/K
local H, Rd, Rv
begin
  if isatt(T,"_FillValue").and..not.isatt(p,"_FillValue") then
     p@_FillValue=T@_FillValue
  end if
  if isatt(T,"_FillValue").and..not.isatt(q,"_FillValue") then
     q@_FillValue=T@_FillValue
  end if
  H=relhum(T,q,p);note this routine expects mixing ratio not specific humidity - however the error is in the third sig. fig.
  H=(/H/100./);convert to fraction from %
  ;Using constants from Kerry's 1994 book on Atmospheric Convection
  Rd=287.04;J/kg/K
  Rv=461.50;J/kg/K
  s=T;set up an array
  s=CP_AIR_C*log(T)-Rd*log(p)+CVAP_WATER_C*q/T-Rv*q*log(H)
  s@long_name="Moist Entropy"
  s@units="J/K"
  return(s)
end

undef("calc_sat_moist_entropy")
function calc_sat_moist_entropy (T,p)
;This function calculates the moist entropy using eq. 4 of Emanuel et al. (2008) in BAMS
;This will be used probably be used to mainly to calculate Chi in Kerry's New GPI index.
;The inputs are: 
;   Temperature in Kelvin,
;   Pressure in Pa
;(it only works if all the inputs are the same type eg all float)
;The equation is s=cp*ln(T)-Rd*ln(p)+Lv*q/T 
;The output is a moist entropy in J/K
local q, Rd, Rv
begin
  ;Calculate the saturation specific humidity
  q=mixhum_ptrh(p/100.,T,conform(T,100.,-1),2)
  ;Using constants from Kerry's 1994 book on Atmospheric Convection
  Rd=287.04;J/kg/K
  Rv=461.50;J/kg/K
  s=T;set up an array
  s=CP_AIR_C*log(T)-Rd*log(p)+CVAP_WATER_C*q/T
  s@long_name=" Saturation Moist Entropy"
  s@units="J/K"
  return(s)
end

undef("calc_equiv_pot_temp")
function calc_equiv_pot_temp (T,p,q)
;This function calculates the equivalent potential tempearture 
;This is after eq. 4.5.11 in Kerry's 1994 book on Atmospheric Convection
;The inputs are: 
;   Temperature in Kelvin,
;   Pressure in Pa
;   Specific Humidity (kg/kg)
;(it only works if all the inputs are the same type eg all float)
;The equation is Theta_e=T*(p0/p)^(Rd/CP_AIR_C)*H^(q*Rv/CP_AIR_C)*exp(CVAP_WATER_C*q/(CP_AIR_C*T)) 
;The output is a temperature at 1000mb in K
local H, Rd, Rv
begin
  if isatt(T,"_FillValue").and..not.isatt(p,"_FillValue") then
     p@_FillValue=T@_FillValue
  end if
  if isatt(T,"_FillValue").and..not.isatt(q,"_FillValue") then
     q@_FillValue=T@_FillValue
  end if
  H=relhum(T,q,p);note this routine expects mixing ratio not specific humidity - however the error is in the third sig. fig.
  H=(/H/100./);convert to fraction from %
  ;Using constants from Kerry's 1994 book on Atmospheric Convection
  p0=100000.;1000mb in Pa
  Rd=287.04;J/kg/K
  Rv=461.50;J/kg/K
  Theta_e=T;set up an array
 ; Theta_e=T*((p0/p)^(Rd/CP_AIR_C))*(H^(-q*Rv/CP_AIR_C))*exp(CVAP_WATER_C*q/(CP_AIR_C*T))
  Theta_e=(T+q*CVAP_WATER_C/CP_AIR_C)*(p0/p)^(Rd/CP_AIR_C)
  Theta_e@long_name="Equivalent Potential Temperature"
  return(Theta_e)
end
