# -*- coding: utf-8 -*-
"""
This is a model of the Sahara that steps through a few years.
"""

import numpy as np
import pylab as plt
import csv
from scipy.interpolate import interp1d

# Read in observed forcing data from file...
with open('BergerLoutre1991.msdos.csv', 'rb') as preccsvfile:
    BL91reader = csv.reader(preccsvfile, dialect='excel')
    for row in BL91reader:
        BL91precc=row[4]

def PRECC(years=0.):
    return(prec)

def EPICA_CO2(years=0.):
    return(co2)


def iterate(years=0.,bkgd_param=0.,precc_param=1.,co2_param=1.,
            fdbk_param=1.,tauv=1.,taun=1.,sigma=1.):
    """
    This function runs sigma.shape instances of the model with set parameters.
    The inputs are the 7 different parameters and the number of years.
        Each of the parameters only be 1d-arrays AND OF THE SAME SIZE!
    """
    #Initialise arrays
       #set the whole of the $\nu$ array to 999 as missing data
    nu=np.ones((sigma.shape,years))*999. 
    nu[:,0]=0. #set the initial conditions as 1 (forest)
    N=np.ones((sigma.shape,years))*999.
    N[:,0]=1. #set the initial conditions of noise as 0
    for yr in range(1,years-1):
        if yr%1000==0:
            print "We're on time %d" % (yr)
        prec=PRECC(yr)
        co2=EPICA_CO2(yr)
        r=(bkgd_param+precc_param*prec(yr))/(co2_param*co2
               )+fdbk_param*nu[:,yr]+N[:,yr]
        nu[:,yr+1]=nu[:,yr]+(np.tanh(r)-nu[:,yr])/tauv
        noise=np.random.normal(0,1,sigma.shape)        
        N[:,yr+1]=N[:,yr]+(sigma*noise[:,yr]-N[:,yr])/taun
        #note. by choosing Dt=1, then the Dt and sqrt(Dt) disappear
        
    return(nu)
    
    