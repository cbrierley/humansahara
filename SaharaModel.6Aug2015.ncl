load "$NCL_DIR/common.ncl"
load "~/Dropbox/HumanSahara/SaharaModel_functions.6Aug2015.ncl"

;;; SETTINGS ;;;;
PLAY_WITH_DEFAULT=False
INITIAL_PARAMETER_SAMPLE=True
VERBOSE=True
ENFORCE_RECALCULATION=False; Be careful setting this as true. It will take time...

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; START OF COMPUTATION ;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Create some piccies and metrics for the default parameter setting
if PLAY_WITH_DEFAULT then
  default_params=(/0.,-50.,1.,1.,5.,5.,1./);bkgd_param,precc_param,co2_param,fdbk_param,tauv,taun,sigma
  default_filename="default/default_params_nu.nc"

  nu_default_cents=get_centennial_averages(default_filename,default_params(0),\
                   default_params(1),default_params(2),default_params(3),default_params(4),\
                   default_params(5),default_params(6),ENFORCE_RECALCULATION,VERBOSE)
  ;read in other variables from file...
  defaultf = addfile(default_filename, "r")
  co2=defaultf->co2
  precc=defaultf->precc
  cents_230ka=co2&time

  ;plot the default timeseries
  plot_single_param_settingx100("default/nu_default_timeseries",\
          "Timeseries of 100 instances of the default parameters",nu_default_cents(0,:,:),cents_230ka,co2,precc)
  
  ;compute some metrics
  pse=compute_sapropel_startends_metric(nu_default_cents,"default_pZse",0.,0.,0.5,ENFORCE_RECALCULATION,VERBOSE)
  p_nsaps=compute_num_sapropel_metric(nu_default_cents,"default_p_nsaps",0.,0.,ENFORCE_RECALCULATION,VERBOSE)  
  print("The proportion having 7 or 8 sapropels is "+(sum(p_nsaps(0,7:8))/100.))
end if ; PLAY_WITH_DEFAULT

;Create initial parameter sample
if INITIAL_PARAMETER_SAMPLE then 
  n_samples=500;n_samples over broad ranges with uniform distribution
  random_setallseed(36484749, 9494847);by setting the seed I know that I'll regenerate the same random settings.
  IPS_bkgd_param=random_uniform(-3.,3.,n_samples)
  IPS_precc_param=random_uniform(-150.,0.,n_samples)
  IPS_co2_param=random_uniform(0.,5.,n_samples)
  IPS_fdbk_param=random_uniform(0.,2.,n_samples)
  IPS_tauv=random_uniform(1.,10.,n_samples)
  IPS_taun=random_uniform(1.,10.,n_samples)
  IPS_sigma=random_uniform(0.,2.,n_samples)
  IPS_filename="IPS/initial_parameter_sample_nu.nc"
  IPS_nsaps_filename="IPS/initial_parameter_sample_nsaps.nc"
  IPS_pZse_filename="IPS/initial_parameter_sample_pZse.nc"
  if VERBOSE then 
    print(" ")
    print("Generated the random numbers")
  end if


  if VERBOSE then 
    begTime = get_cpu_time()
  end if
  IPS_nu=get_centennial_averages(IPS_filename,IPS_bkgd_param,\
                   IPS_precc_param,IPS_co2_param,IPS_fdbk_param,IPS_tauv,\
                   IPS_taun,IPS_sigma,False,VERBOSE)
  if VERBOSE then
    print(" Time to read in or calculate averages: " + (get_cpu_time() - begTime)+ " seconds")
  end if 

  ;read in other variables from file...
  IPSf = addfile(IPS_filename, "r")
  co2=IPSf->co2
  precc=IPSf->precc

  if VERBOSE then 
    begTime = get_cpu_time()
    print(" ")
    print("Computing number of Sapropel metrics")
  end if
  isSapThreshold=0.
  IPS_nsaps=compute_num_sapropel_metric(IPS_nu,IPS_nsaps_filename,IPS_bkgd_param,\
            isSapThreshold,ENFORCE_RECALCULATION,VERBOSE)
  if VERBOSE then
    print(" Time to compute number of Sapropel metrics: " + (get_cpu_time() - begTime)+ " seconds")
  end if 
  IPS_p_nsaps=new((/500,16/),float)
  IPS_p_nsaps@long_name="Proportion with sapropel quantity"
  IPS_p_nsaps!0="parameter_setting"
  IPS_p_nsaps!1="nsaps"
  nsaps_array=ispan(0,15,1)
  nsaps_array@long_name="Number of sapropels between 230-0ka"
  IPS_p_nsaps&nsaps=nsaps_array
  IPS_p_nsaps@isSapThreshold=isSapThreshold
  do i=0,15
    IPS_p_nsaps(:,i)=dim_num(IPS_nsaps.eq.i)
  end do

  ;Compute some metrics for these ensemble members
  if VERBOSE then 
    begTime = get_cpu_time()
    print(" ")
    print("Computing Start/End metrics")
  end if
  IPS_pZse=compute_sapropel_startends_metric(IPS_nu,IPS_pZse_filename,IPS_bkgd_param,0.,0.5,ENFORCE_RECALCULATION,VERBOSE)
  if VERBOSE then
    print(" Time to compute Start/End metrics: " + (get_cpu_time() - begTime)+ " seconds")
  end if 

  seven_saps_as_most_prob=ind(IPS_p_nsaps(:,7).ge.50)
  print("Only "+dimsizes(seven_saps_as_most_prob)+" have 7 sapropels as the most likely number.")


;  do i=0,dimsizes(seven_saps_as_most_prob)-1
;    filename="IPS/IPS_Ens_Mem_"+seven_saps_as_most_prob(i)
;    if .not.fileexists(filename+".pdf") then
;      title_str="IPS Ens_Mem "+seven_saps_as_most_prob(i)+" ("+IPS_bkgd_param(seven_saps_as_most_prob(i))+", "+\
;        IPS_precc_param(seven_saps_as_most_prob(i))+", "+IPS_co2_param(seven_saps_as_most_prob(i))+",~C~"+\
;        IPS_fdbk_param(seven_saps_as_most_prob(i))+", "+IPS_tauv(seven_saps_as_most_prob(i))+", "+\
;        IPS_taun(seven_saps_as_most_prob(i))+", "+IPS_sigma(seven_saps_as_most_prob(i))+")"
;      plot_single_param_settingx100(filename,title_str,\
;            IPS_nu(seven_saps_as_most_prob(i),:,:),co2&time,co2,precc)
;    end if
;  end do

  ;;So where do these lie in parameter space
  ;;I can't visualise a seven dimensional space, so I'll have to use a vast number of slices...
  ;avg_nsaps=dim_sum(IPS_p_nsaps*conform(IPS_p_nsaps,IPS_p_nsaps&nsaps,1))/100.
  ;plot_full_param_space("IPS/IPS_nsaps_param_space","Initial Parameter Space",IPS_bkgd_param,\
  ;                      IPS_precc_param,IPS_co2_param,IPS_fdbk_param,IPS_tauv,IPS_taun,IPS_sigma,avg_nsaps)

  ;Perhaps my metrics aren't the best...
  odp968_tab=readAsciiTable("~/Documents/obs/PaleoObs/Pleistocene/Konijnendijk-etal_2014.tab.txt",5,"float",26)
  odp968_TiAl=odp968_tab(:788,4)/odp968_tab(:788,3);calculate Ti/Al ratio upto ~250ka
  odp968_chron=odp968_tab(:788,2);the chronology upto ~250ka
  time=IPS_nu&time
  time_flip=time(::-1)
  ;print(time_flip(0:5))
  tial_tmp=linint1(odp968_chron,odp968_TiAl,False,time_flip,0)
  TiAl=tial_tmp(::-1)
  TiAl!0="time"
  TiAl&time=IPS_nu&time
  ;TiAl=where(TiAl.gt.0.75,0.75,TiAl)
  ;now compute the correlation coefficient between the model timeseries and this observed one.
  ;  Not including any lags, and only going up to 20ka (to avoid including holocene in calibration set)
  pre20ka=ind(IPS_nu&time.gt.20)
  IPS_correl=escorc(TiAl(pre20ka),IPS_nu(:,:,pre20ka))
  IPS_correl_signif=rtest(IPS_correl,dimsizes(TiAl(pre20ka)),0)
  print("Signif correlation for "+num(IPS_correl_signif.lt.0.001)+" instances")
  ;I'm just going to use the correlation as weighting directly - so set insignifcant to 0
  IPS_correl=where(IPS_correl_signif.ge.0.001,0,IPS_correl)
  ;I'm also going to ignore any model that doesn't have between 5-9 sapropels...
  IPS_correl=where((IPS_nsaps.ge.5).and.(IPS_nsaps.le.9),IPS_correl,0)
  print("I'm only considering "+num(IPS_correl^2.gt.0)+" instances")

  ;plot_weighted_timeseries("IPS/IPS_correl_subset_avg_ts","IPS Subset distribution",IPS_nu,IPS_correl^2)
  plot_holocene_collapse("IPS/IPS_correl_subset_Holocene","IPS Subset",IPS_nu,IPS_correl^2)
  plot_wgt_holocene_collapse("IPS/IPS_correl_wgt_Holocene","Correlation Weighted Probabilities",IPS_nu,IPS_correl^2)
end if ; INITIAL_PARAMETER_SAMPLE
